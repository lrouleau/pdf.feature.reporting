package pdf.feature.reporting;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import fr.rouleau.featurereporting.FeatureReader;
import fr.rouleau.featurereporting.WrapperFeature;

public class FeatureReaderTest {

	@Test(expected = IllegalArgumentException.class)
	public void given_not_existing_dir_when_read_file_then_thow_exception() throws Exception {
		FeatureReader featureReader = new FeatureReader();
		featureReader.read("/nonexistingdirectory");
	}

	@Test
	public void given_any_config_file_then_thow_exception() throws Exception {
		FeatureReader featureReader = new FeatureReader();
		List<WrapperFeature> read = featureReader.read("/featuretest");
		Assert.assertEquals(1, read.size());
	}

}