package pdf.feature.reporting;

import org.junit.Assert;
import org.junit.Test;

import fr.rouleau.featurereporting.Config;

public class ConfigTest {

	@Test(expected = IllegalArgumentException.class)
	public void given_any_config_file_then_thow_exception() throws Exception {
		Config.builderFromFile("/nonExistingFile.json").build();
	}

	@Test()
	public void given_a_config_file_when_search_title_then_return_atitle() throws Exception {
		Assert.assertEquals("atitle", Config.builderFromFile("/configtest.json").build().getTitle());
	}

}
