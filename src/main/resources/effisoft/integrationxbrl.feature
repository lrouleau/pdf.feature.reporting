@integrationxbrl
@order=2
@creation=20171231/v5.0.2
Feature: Pivot File Integration  

	Intégration des fichiers pivots
- soit unitairement (en précisant, via l'IHM, la taxonomie et le nom de l'état)
- soit en masse via fichier zip. (avec des fichiers correctement nommés)

Background: 
	Given my browser is open on login page 
	When I log with login "assweb" and as password "superadmin" 
	
Scenario: Vérification des composants dans l'écran 
	When I click on pivot file link
	Then Component "the-pivot-files-upload" is available 
	Then Component "the-pivot-files-list" is available 
	
Scenario: Excel file integration 
#	When I click on perimeter link
#	When je sélectionne le modèle "Assurance non vie" 
#	When je sélectionne la société "GANIA - Gan Assurances IARD" 
#	When je sélectionne l'exercice "2017" 
#	When je sélectionne le mois de "Décembre" 
#	When je sélectionne le plan comptable "Solvency 2" 
	When i click on pivot file link
	When i integrate the file "S2-2016-Q4-AES_E.01.01.xlsx" in the inputfile
 	When i select "Solvabilité II - ECB - annuel" in the taxonomy field
	When i select "E.01.01.16 - Deposits to cedants - line-by-line reporting" in the state field
	When i click on the valid button
	Then the file is added to the list
	
Scenario: Zip file integration
#	When I click on perimeter link
#	When je sélectionne le modèle "Assurance non vie" 
#	When je sélectionne la société "GANIA - Gan Assurances IARD" 
#	When je sélectionne l'exercice 2017 
#	When je sélectionne le mois de "Décembre" 
#	When je sélectionne le plan comptable "Solvency 2" 
	When i click on pivot file link
	When i integrate the file "fichiers_pivots.zip" in the inputfile
	Then "2" files are added to the list

Scenario: Excel file integration without informed required fields 
#	When I click on perimeter link
#	When je sélectionne le modèle "Assurance non vie" 
#	When je sélectionne la société "GANIA - Gan Assurances IARD" 
#	When je sélectionne l'exercice 2017 
#	When je sélectionne le mois de "Décembre" 
#	When je sélectionne le plan comptable "Solvency 2" 
	When i click on pivot file link
	When i integrate the file "S2-2016-Q4-AES_E.01.01.xlsx" in the inputfile	
	Then the validate button is disabled
	
#Scenario: Intégration d'un fichier pdf - au mauvais format 
#	When I click on perimeter link
#	When je sélectionne le modèle "Assurance non vie" 
#	When je sélectionne la société "GANIA - Gan Assurances IARD" 
#	When je sélectionne l'exercice 2017 
#	When je sélectionne le mois de "Décembre" 
#	When je sélectionne le plan comptable "Solvency 2" 
#	When i click on pivot file link
#	When j'intègre le fichier "fichier_erreur.pdf" 
#	Then j'ai un message d'erreur qui s'affiche et m'indiquant que le fichier n'est pas au bon format 

#Scenario: Intégration d'un fichier zip contenant un fichier pdf - au mauvais format 
#	When I click on perimeter link
#	When je sélectionne le modèle "Assurance non vie" 
#	When je sélectionne la société "GANIA - Gan Assurances IARD" 
#	When je sélectionne l'exercice 2017 
#	When je sélectionne le mois de "Décembre" 
#	When je sélectionne le plan comptable "Solvency 2" 
#	When je clic sur l'item du menu "REPORTING REGLEMENTAIRE > Intégration des fichiers pivots" 
#	When j'intègre le fichier "fichier_erreur.zip" 
#	Then j'ai un message d'erreur qui s'affiche et m'indiquant que le fichier contenu dans le zip n'est pas au bon format 

#Scenario: Téléchargement du fichier pivot intégré 
#	When I click on perimeter link
#	When je sélectionne le modèle "Assurance non vie" 
#	When je sélectionne la société "GANIA - Gan Assurances IARD" 
#	When je sélectionne l'exercice 2017 
#	When je sélectionne le mois de "Décembre" 
#	When je sélectionne le plan comptable "Solvency 2" 
#	When je clic sur l'item du menu "REPORTING REGLEMENTAIRE > Intégration des fichiers pivots" 
#	When j'intègre le fichier pivot "fichier_pivot_S.02.01.01.xlsx" à intégtrer sur l'emplacement à définir 
#	When je sélectionne la valeur "Solvabilité 2 - Annuelle" dans le champ "Taxonomie" 
#	When je sélectionne la valeur "S.02.01.01 - BS-C1A - Balance sheet (annual)" dans le champ "Etat" 
#	When je clique sur "charger en base" 
#	When Je clique sur le lien de téléchargement du fichier source "fichier_pivot_S.02.01.01.xlsx" 
#	Then le fichier s'ouvre 
#	Then le champ "généré par Assuretat" est décoché
