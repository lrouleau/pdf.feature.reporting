@order=1
@creation=20171231/v5.0.2
Feature: Accounting period page 

Background: 
	Given my browser is open on accounting period page 
	
Scenario: No template selected 
	Then the button create should be disabled 
	Then the button refresh should be invisible 
	Then exercise should be empty 
	
Scenario: Select a template 
	When i select the template 1 
	Then the button exercise should display a date 
	Then the button create should be enabled 
	Then the button refresh should be invisible 
	
Scenario: Create a period 
	Given template 1 is selected 
	When i submit the creation form 
	Then the toaster should display "Création du modèle" or "Creating template" 
	Then the button create should be disabled 
	Then the button refresh should be visible 
	Then I wait for the end of process 
