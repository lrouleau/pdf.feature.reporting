Feature: Contact window 

	On login page, a contact link should be visible.
   A click on this link displays a contact modal window.

Scenario: Contact window 
	Given my browser is open on login page 
	When I click the contact link 
	Then a modal window should display 2 contacts blocks 
 