Feature: Menu page 

Background: 
	Given my browser is open on login page 
	When I log with login "assweb" and as password "superadmin" 
	
Scenario: Check menu headers 
	Then menu should display a header "admin" 
	Then menu should display a header "repository" 
	Then menu should display a header "reporting" 
