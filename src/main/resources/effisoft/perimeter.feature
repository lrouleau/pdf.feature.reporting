@perimeter
Feature: Perimeter

Le périmètre courant est affiché dans un footer de l'application.
On peut modifier le perimetre via une fenetre "popup" accessible depuis le menu "outils", item "perimetre".
A la validation, le contenu du footer est mis à jour.

Background: 
	Given i am connected with login "assweb" and password "superadmin"

Scenario: Vérifier que la popup de périmètre a un contenu cohérent
 When i click on Perimeter menu
# Then the popup is displayed
# Then the model "Assurance non vie" is available
# Then the model "Assurance mixte GANP" is available
# Then the 12 months of the year are available
 Then the charts of accounts "IFRS" is available
# Then the charts of accounts "Local GAAP" is available
# Then the charts of accounts "Solvency 2" is available
#
#Scenario: GANIA exist in no life Insurance
# When i click on Perimeter menu
# When i select the model "Assurance non vie"
# Then the company "GANIA" is available
# Then U/W year "2017" is available
#
#Scenario: GANP exist in no life Insurance
# When i click on Perimeter menu
# When i select the model "Assurance mixte GANP"
# Then the company "GANP - Gan Prévoyance" is available
# Then U/W year "2016" is available
#
#Scenario:
# When i click on Perimeter menu
# When i select the model "Assurance mixte GANP"
# When i select the company "GANP - Gan Prévoyance"
# When i select U/W year "2016"
# When i select the month "Juin"
# When i select the charts of accounts "Solvency 2"
# When i submit the form
# Then the perimeter display the model "Assurance mixte GANP"
# Then the perimeter display the company "GANP - Gan Prévoyance"
# Then the perimeter display U/W year "2016"
# Then the perimeter display the month "Juin"
# Then the perimeter display the charts of accounts "Solvency 2"
#
#Scenario:
# When i click on Perimeter menu
# When i select the model "Assurance non vie"
# When i select the company "GANIA - Gan Assurances IARD"
# When i select U/W year "2017" 
# When i select the month "Décembre"
# When i select the charts of accounts "Local GAAP"
# When i submit the form
# Then the perimeter display the model "Assurance non vie"
# Then the perimeter display the company "GANIA - Gan Assurances IARD"
# Then the perimeter display U/W year "2017"
# Then the perimeter display the month "Décembre"
# Then the perimeter display the charts of accounts "Local GAAP"
