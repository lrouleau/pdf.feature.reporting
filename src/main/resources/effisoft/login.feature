Feature: Login page 

Background: 
	Given my browser is open on login page 
	
Scenario: Refuse invalid credentials 
	Given I use as login "wrongassweb" and as password "******" 
	When I submit the login form 
	Then a toaster should appear 
	
Scenario: Accept valid credentials 
	Given I use as login "assweb" and as password "superadmin" 
	When I submit the login form 
	Then the page should go to home page 
	
Scenario: Retry connexion after typing a bad password 
	Given I use as login "ass" and as password "super" 
	Given I submit the login form 
	Given I use as login "web" and as password "admin" 
	When I submit the login form 
	Then the page should go to home page 
 