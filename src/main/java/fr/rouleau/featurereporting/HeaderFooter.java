package fr.rouleau.featurereporting;

import java.io.IOException;

import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.ElementList;
import com.itextpdf.tool.xml.XMLWorkerHelper;

public class HeaderFooter extends PdfPageEventHelper {

	public static final String HEADER = "";
	public static final String FOOTER = "";

	protected ElementList header;
	protected ElementList footer;

	public HeaderFooter() throws IOException {
		header = XMLWorkerHelper.parseToElementList(HEADER, null);
		footer = XMLWorkerHelper.parseToElementList(FOOTER, null);
	}

	@Override
	public void onEndPage(PdfWriter writer, Document document) {
		PdfContentByte cb = writer.getDirectContent();
		//Phrase header = new Phrase("this is a header");
		Phrase footer = new Phrase(" - " + Integer.toString(1 + document.getPageNumber()) + " - ");
		//ColumnText.showTextAligned(cb, Element.ALIGN_CENTER, header,(document.right() - document.left()) / 2 + document.leftMargin(), document.top() + 10, 0);
		ColumnText.showTextAligned(cb, Element.ALIGN_CENTER, footer,(document.right() - document.left()) / 2 + document.leftMargin(), document.bottom() - 10, 0);
	}

}