package fr.rouleau.featurereporting;

import java.io.File;
import java.io.FileReader;
import java.net.URL;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class ConfigBuilder {

	private String fileName;
	
	public Config build() throws Exception {
		Config config = new Config();
		JSONParser jsonParser = new JSONParser();
		URL resource = this.getClass().getResource(getFileName());
		if (resource==null) {
			throw new IllegalArgumentException(getFileName() + " not found in resources folder");
		}
		try (FileReader reader = new FileReader(new File(resource.toURI()))) {
			config.setJSon((JSONObject) jsonParser.parse(reader));
		}
		return config;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	
	

}
