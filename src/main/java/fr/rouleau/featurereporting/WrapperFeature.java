package fr.rouleau.featurereporting;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import gherkin.ast.Feature;
import gherkin.ast.ScenarioDefinition;
import gherkin.ast.Tag;

public class WrapperFeature {

	private Feature feature;
	private Integer order;
	private LocalDate date;

	public WrapperFeature(Feature feature) {
		setFeature(feature);
		setOrder(getOrder(feature));
		setDate(getDate(feature));
	}

	private int getOrder(Feature feature) {
		for (Tag tag : feature.getTags()) {
			String[] split = tag.getName().split("=");
			if (split.length == 2 && "@order".equalsIgnoreCase(split[0])) {
				return Integer.parseInt(split[1]);
			}
		}
		return 10000;
	}

	private LocalDate getDate(Feature feature) {
		for (Tag tag : feature.getTags()) {
			String[] split = tag.getName().split("=");
			if (split.length == 2 && split[0].equals("creation")) {
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("ddMMyyyy");
				return LocalDate.parse(split[1], formatter);
			}
		}
		return null;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public Feature getFeature() {
		return feature;
	}

	public void setFeature(Feature feature) {
		this.feature = feature;
	}

	public Integer getOrder() {
		return order;
	}

	public void setOrder(Integer order) {
		this.order = order;
	}

	public List<Tag> getTags() {
		return feature.getTags();
	}

	public String getDescription() {
		return feature.getDescription();
	}

	public String getName() {
		return feature.getName();
	}

	public List<ScenarioDefinition> getChildren() {
		return feature.getChildren();
	}

}
