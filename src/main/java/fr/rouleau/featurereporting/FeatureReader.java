package fr.rouleau.featurereporting;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import cucumber.runtime.FeatureBuilder;
import cucumber.runtime.io.FileResource;
import cucumber.runtime.model.CucumberFeature;

public class FeatureReader {

	public List<WrapperFeature> read(String name) throws URISyntaxException {

		List<WrapperFeature> features = new ArrayList<>();

		List<CucumberFeature> cucumberFeatures = new ArrayList<>();
		FeatureBuilder featureBuilder = new FeatureBuilder(cucumberFeatures);

		URL resource = this.getClass().getResource(name);
		
		if (resource==null) {
			throw new IllegalArgumentException(name + " not found in classpath");
		}
		
		List<Path> readDir = readDir(Paths.get(resource.toURI()));

		for (Iterator<Path> iterator = readDir.iterator(); iterator.hasNext();) {
			Path path = iterator.next();
			File file = new File(path.toAbsolutePath().toString());
			FileResource fileResource = FileResource.createClasspathFileResource(file.getParentFile(), file);
			featureBuilder.parse(fileResource);
		}

		for (CucumberFeature feature : cucumberFeatures) {
			features.add(new WrapperFeature(feature.getGherkinFeature().getFeature()));
		}

		features.sort(new Comparator<WrapperFeature>() {
			@Override
			public int compare(WrapperFeature o1, WrapperFeature o2) {
				int compareTo = o1.getOrder().compareTo(o2.getOrder());
				if (compareTo != 0) {
					return compareTo;
				}
				if (o1.getName() == null) {
					return -1;
				}
				return o1.getName().compareTo(o2.getName());
			}
		});

		return features;

	}

	private List<Path> readDir(Path rootPath) {
		List<Path> paths = new ArrayList<>();
		if (Files.isDirectory(rootPath)) {
			try (DirectoryStream<Path> stream = Files.newDirectoryStream(rootPath)) {
				for (Path path : stream) {
					if (Files.isDirectory(rootPath)) {
						paths.addAll(readDir(path));
					}
				}
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		} else {
			if (rootPath.toString().endsWith(".feature")) {
				paths.add(rootPath);
			}
		}
		return paths;
	}

}