package fr.rouleau.featurereporting;

import java.util.Arrays;
import java.util.List;

import org.json.simple.JSONObject;

public class Config {

	private JSONObject json;

	protected Config() {
		super();
	}

	public static ConfigBuilder builderFromFile(String fileName) {
		ConfigBuilder configBuilder = new ConfigBuilder();
		configBuilder.setFileName(fileName);
		return configBuilder;
	}

	public void setJSon(JSONObject jsonObject) {
		setJson(jsonObject);

	}

	public JSONObject getJson() {
		return json;
	}

	public void setJson(JSONObject json) {
		this.json = json;
	}

	public String getTitle() {
		return (String) json.get("title");

	}

	public String getVersion() {
		return (String) json.get("version");
	}
	
	public List<String> getAdress() {
		String allAddress = (String) json.get("address");
		return Arrays.asList(allAddress.split("--"));
	}

	public String getImage() {
		JSONObject jsonObject = (JSONObject) json.get("image");
		return (String) jsonObject.get("src");
	}

	public float getAbsolutePositionImageX() {
		JSONObject jsonObject = (JSONObject) json.get("image");
		JSONObject position = (JSONObject) jsonObject.get("absoluteposition");
		return Float.parseFloat((String) position.get("x"));
	}

	public float getAbsolutePositionImageY() {
		JSONObject jsonObject = (JSONObject) json.get("image");
		JSONObject position = (JSONObject) jsonObject.get("absoluteposition");
		return Float.parseFloat((String) position.get("y"));
	}
	
	public String getFileName() {
		return (String) json.get("fileName");
	}

}
