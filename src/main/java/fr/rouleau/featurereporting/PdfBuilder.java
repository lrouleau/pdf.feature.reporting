package fr.rouleau.featurereporting;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfCopy;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfWriter;

import gherkin.ast.ScenarioDefinition;
import gherkin.ast.Step;
import gherkin.ast.Tag;

public class PdfBuilder {

	public static final String RESULT = "test.pdf";

	private Map<String, Font> fonts;
	private Config config;
	private PdfWriter writer;
	private Document document;

	public PdfBuilder() throws Exception {
		setFonts(FontBuilder.fromCodeDefinition());
		setConfig(Config.builderFromFile("/config.json").build());
	}

	private void addCover() throws IOException, DocumentException, FileNotFoundException {
		PdfReader cover = new PdfReader("cover.pdf");
		PdfReader reader = new PdfReader("test.pdf");
		Document document = new Document();
		PdfCopy copy = new PdfCopy(document, new FileOutputStream(getConfig().getFileName()));
		document.open();
		copy.addDocument(cover);
		copy.addDocument(reader);
		document.close();
		cover.close();
		reader.close();
	}

	private void createSecondPage() throws DocumentException, MalformedURLException, IOException {
		getDocument().add(new Paragraph(getConfig().getTitle(), getFonts().get("fontSecondPage")));
		getDocument().add(Chunk.NEWLINE);
		getDocument().add(new Paragraph("Version: " + getConfig().getVersion(), getFonts().get("fontSecondPage")));
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d-MMM-yyyy");
		getDocument().add(new Paragraph("Generated: " + formatter.format(LocalDate.now()), getFonts().get("fontSecondPage")));
		getDocument().add(Chunk.NEWLINE);
		for (String add : getConfig().getAdress()) {
			getDocument().add(new Paragraph(add, getFonts().get("fontSecondPage")));
		}
		getDocument().add(Chunk.NEWLINE);
		addImage();
		getDocument().newPage();
	}

	public void createPdf(List<WrapperFeature> readFeature) throws IOException, DocumentException, SQLException {
		createDocument();
		createSecondPage();
		for (Iterator<WrapperFeature> iterator = readFeature.iterator(); iterator.hasNext();) {
			WrapperFeature feature = iterator.next();
			createPage(feature);
			addImage();
			if (iterator.hasNext()) {
				getDocument().newPage();
			}
		}
		getDocument().close();
		addCover();
	}

	private void addImage() throws BadElementException, MalformedURLException, IOException, DocumentException {
		Image image = Image.getInstance(getConfig().getImage());
		image.setAbsolutePosition(getConfig().getAbsolutePositionImageX(), getConfig().getAbsolutePositionImageY());
		getDocument().add(image);
	}

	private void createPage(WrapperFeature feature) throws DocumentException {
		addTags(feature);
		if (!feature.getTags().isEmpty()) {
			getDocument().add(Chunk.NEWLINE);
		}
		addFeatureTitle(feature);
		if (feature.getDescription() != null) {
			Paragraph element = new Paragraph(feature.getDescription(), fonts.get("fontDescription"));
			getDocument().add(element);
			getDocument().add(Chunk.NEWLINE);
		}
		for (ScenarioDefinition scenarioDefinition : feature.getChildren()) {
			addScenarioDefinition(scenarioDefinition);
		}
	}

	private void addTags(WrapperFeature feature) throws DocumentException {
		for (Tag tag : feature.getTags()) {
			String name = tag.getName();
			if (name.startsWith("@order=")) {
				addTagOrder(name);
			} else if (name.startsWith("@creation=")) {
				addTagCreation(name);
			} else {
				addTag(name);
			}
		}
	}

	private void addTagCreation(String name) throws DocumentException {
		String[] split = name.split("=");
		String[] detail = split[1].split("/");

		String pattern = "yyyyMMdd";
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
		LocalDate localDate;
		try {
			localDate = LocalDate.parse(detail[0], formatter);
		} catch (DateTimeException dte) {
			throw new IllegalArgumentException(name + " must respect pattern: " + pattern);
		}

		DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("d-MMM-yyyy");

		getDocument().add(new Paragraph("# Since " + formatter2.format(localDate) + " and Version: " + detail[1],
				fonts.get("fontTag")));
	}

	private void addTagOrder(String name) {
	}

	private void addTag(String name) throws DocumentException {
		getDocument().add(new Paragraph(name, fonts.get("fontTag")));
	}

	private void addScenarioDefinition(ScenarioDefinition scenarioDefinition) throws DocumentException {
		Paragraph element = new Paragraph();
		element.add(new Phrase(scenarioDefinition.getKeyword() + " ", fonts.get("fontFeature")));
		getDocument().add(element);
		for (Step step : scenarioDefinition.getSteps()) {
			addStep(step);
		}
		getDocument().add(Chunk.NEWLINE);
	}

	private void addStep(Step step) throws DocumentException {
		Paragraph elementStep = new Paragraph();
		elementStep.add(new Phrase(step.getKeyword() + " ", fonts.get("fontStepKeyword")));
		String[] split = step.getText().split("\"");
		for (int i = 0; i < split.length; i++) {
			String str = split[i];
			if (i % 2 == 0) {
				elementStep.add(new Phrase(str, fonts.get("fontStepText")));
			} else {
				elementStep.add(new Phrase("\"" + str + "\"", fonts.get("fontStepTextStr")));
			}
		}
		getDocument().add(elementStep);
	}

	private void addFeatureTitle(WrapperFeature feature) throws DocumentException {
		Paragraph element = new Paragraph();
		element.add(new Phrase("Feature ", fonts.get("fontFeature")));
		element.add(new Phrase(feature.getName()));
		getDocument().add(element);
		getDocument().add(Chunk.NEWLINE);
	}

	private void createDocument() throws DocumentException, FileNotFoundException, IOException {
		setDocument(new Document(PageSize.A4));
		setWriter(PdfWriter.getInstance(getDocument(), new FileOutputStream(RESULT)));
		getDocument().open();
		HeaderFooter headerFooter = new HeaderFooter();
		getWriter().setPageEvent(headerFooter);
		headerFooter.onStartPage(getWriter(), getDocument());
		getWriter().setPdfVersion(PdfWriter.VERSION_1_5);
	}

	public Map<String, Font> getFonts() {
		return fonts;
	}

	public void setFonts(Map<String, Font> fonts) {
		this.fonts = fonts;
	}

	public Config getConfig() {
		return config;
	}

	public void setConfig(Config config) {
		this.config = config;
	}

	public PdfWriter getWriter() {
		return writer;
	}

	public void setWriter(PdfWriter writer) {
		this.writer = writer;
	}

	public Document getDocument() {
		return document;
	}

	public void setDocument(Document document) {
		this.document = document;
	}

	public void generatePdf() throws URISyntaxException, IOException, DocumentException, SQLException {
		FeatureReader featureReader = new FeatureReader();
		List<WrapperFeature> features = featureReader.read("/effisoft");
		createPdf(features);
		Files.delete(Paths.get("test.pdf"));
	}
}
