package fr.rouleau.featurereporting;

import java.util.HashMap;
import java.util.Map;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;

public class FontBuilder {

	private FontBuilder() {
	}

	public static Map<String, Font> fromCodeDefinition() {
		Map<String, Font> fonts = new HashMap<>();
		fonts.put("fontFeature", new Font(FontFamily.HELVETICA, 13, Font.ITALIC, BaseColor.BLUE));
		fonts.put("fontDescription", new Font(FontFamily.HELVETICA, 11, Font.ITALIC, BaseColor.GRAY));
		fonts.put("fontStepKeyword", new Font(FontFamily.HELVETICA, 12, Font.ITALIC, BaseColor.BLUE));
		fonts.put("fontStepText", new Font(FontFamily.HELVETICA, 11, Font.NORMAL, BaseColor.BLACK));
		fonts.put("fontStepTextStr", new Font(FontFamily.HELVETICA, 11, Font.ITALIC, BaseColor.GRAY));
		fonts.put("fontTag", new Font(FontFamily.HELVETICA, 11, Font.NORMAL, BaseColor.GRAY));
		fonts.put("fontSecondPage", new Font());
		return fonts;
	}

}
